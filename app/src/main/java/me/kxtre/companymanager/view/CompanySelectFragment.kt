package me.kxtre.companymanager.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import me.kxtre.companymanager.R
import me.kxtre.companymanager.databinding.DialogAddCompanyBinding
import me.kxtre.companymanager.databinding.FragmentCompanySelectBinding
import me.kxtre.companymanager.model.Company
import me.kxtre.companymanager.viewmodel.CompanySelectViewModel

class CompanySelectFragment : Fragment(), LifecycleOwner {

    private lateinit var viewModel: CompanySelectViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentCompanySelectBinding.inflate(layoutInflater)
        populateData(binding)
        return binding.root
    }

    private fun showAddCompanyDialog() {
        val context = context ?: return
        val company = Company()

        val dialogView = createCompanyDialogView(company)
        val dialogBuilder = MaterialAlertDialogBuilder(context)
            .setTitle(R.string.add_company)
            .setView(dialogView)
            .setPositiveButton(R.string.create) { _, _ ->
                viewModel.add(company)
            }
        dialogBuilder.show()
    }

    private fun createCompanyDialogView(company: Company): View {
        val binding = DialogAddCompanyBinding.inflate(layoutInflater)
        binding.company = company
        return binding.root
    }

    private fun populateData(binding: FragmentCompanySelectBinding) {
        viewModel = CompanySelectViewModel(binding,
            onCompanySelectedListener = {
            findNavController().navigate(
                R.id.action_companySelectFragment_to_companyFragment
            )
        },
        onAddCompanySelected = {
            showAddCompanyDialog()
        })
    }
}