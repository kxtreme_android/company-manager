package me.kxtre.companymanager.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.kxtre.companymanager.R
import me.kxtre.companymanager.databinding.FragmentModuleClientBinding

class ModuleClientFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentModuleClientBinding.inflate(layoutInflater, container, false)
        return binding.root
    }
}