package me.kxtre.companymanager.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import me.kxtre.companymanager.R
import me.kxtre.companymanager.controller.StateController
import me.kxtre.companymanager.databinding.FragmentCompanyBinding
import me.kxtre.companymanager.viewmodel.CompanySelectViewModel
import me.kxtre.companymanager.viewmodel.CompanyViewModel

class CompanyFragment : Fragment() {

    private lateinit var viewModel: CompanyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       val binding = FragmentCompanyBinding.inflate(layoutInflater, container, false)
        populateData(binding)
        return binding.root
    }

    private fun populateData(binding: FragmentCompanyBinding) {

        val company = StateController.company
        if(company == null) {
            Toast.makeText(context, R.string.error_missing_company, Toast.LENGTH_SHORT).show()
            findNavController().popBackStack()
            return
        }
        viewModel = CompanyViewModel(binding, company)
    }

}