package me.kxtre.companymanager

import android.app.Application
import android.content.res.Resources

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        res = resources
    }
    companion object {
        lateinit var res: Resources
    }
}
