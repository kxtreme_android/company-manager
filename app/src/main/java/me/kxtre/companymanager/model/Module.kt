package me.kxtre.companymanager.model

import androidx.annotation.*

class Module(
    @IdRes val menuId: Int,
    @StringRes val name: Int,
    @DrawableRes val icon: Int
) {
}