package me.kxtre.companymanager.model

import androidx.databinding.BaseObservable
import androidx.databinding.ObservableArrayList

class Company: BaseObservable() {
    var name: String = ""
    val clients = ObservableArrayList<Client>()
}

