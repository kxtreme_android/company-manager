package me.kxtre.companymanager.model

import androidx.databinding.BaseObservable

class Client: BaseObservable() {
    var name: String = ""
}