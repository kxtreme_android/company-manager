package me.kxtre.companymanager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.kxtre.companymanager.databinding.ListItemCompanyBinding
import me.kxtre.companymanager.model.Company

class CompanySelectAdapter : RecyclerView.Adapter<CompanySelectAdapter.ViewHolder>() {
    private var companies = listOf<Company>()
    private var companySelectedListener: (company: Company) -> Unit = {}
    class ViewHolder(val binding: ListItemCompanyBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemCompanyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = holder.binding
        binding.company = companies[position]
        binding.root.setOnClickListener { companySelectedListener(companies[position]) }

    }

    override fun getItemCount(): Int {
        return companies.count()
    }

    fun setCompanies(companies: List<Company>) {
        this.companies = companies
    }

    fun setClickedListener(listener: (company: Company) -> Unit) {
        companySelectedListener = listener
    }

}