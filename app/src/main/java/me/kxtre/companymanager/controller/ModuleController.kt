package me.kxtre.companymanager.controller

import me.kxtre.companymanager.R
import me.kxtre.companymanager.model.Module

object ModuleController {
    val availableModules = listOf(
        Module(R.id.module_clients, R.string.clients, R.drawable.ic_baseline_supervised_user_circle_24)
    )
}