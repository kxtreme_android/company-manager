package me.kxtre.companymanager.viewmodel

import me.kxtre.companymanager.databinding.FragmentModuleClientBinding

class ModuleClientViewModel(
    val binding: FragmentModuleClientBinding,
    private val onAddClientSelected: () ->Unit
) {

    init {
        binding.extendedFab.setOnClickListener {
            onAddClientSelected()
        }
    }
}