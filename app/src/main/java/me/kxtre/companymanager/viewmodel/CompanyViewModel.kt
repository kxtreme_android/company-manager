package me.kxtre.companymanager.viewmodel

import android.view.Menu
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import me.kxtre.companymanager.App
import me.kxtre.companymanager.controller.ModuleController
import me.kxtre.companymanager.controller.StateController
import me.kxtre.companymanager.databinding.FragmentCompanyBinding
import me.kxtre.companymanager.model.Company

class CompanyViewModel(
    val binding: FragmentCompanyBinding,
    val company: Company
) {

    init {
        binding.root.post {
            binding.bottomNavigation.setupWithNavController(
                binding.fragmentContainerView2.findNavController()
            )
        }
        redrawMenu()
    }

    private fun redrawMenu() {
        val menu = binding.bottomNavigation.menu
        menu.clear()
        ModuleController.availableModules.forEach {
            menu.add(0, it.menuId, 0, App.res.getString(it.name))
                .setIcon(it.icon)
        }

    }
}