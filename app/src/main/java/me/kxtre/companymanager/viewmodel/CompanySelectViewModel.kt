package me.kxtre.companymanager.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import me.kxtre.companymanager.controller.StateController
import me.kxtre.companymanager.adapter.CompanySelectAdapter
import me.kxtre.companymanager.databinding.FragmentCompanySelectBinding
import me.kxtre.companymanager.model.Company

class CompanySelectViewModel(private val binding: FragmentCompanySelectBinding,
                             private val onCompanySelectedListener: (company: Company)->Unit,
                             private val onAddCompanySelected: () ->Unit
): ViewModel() {

    private var adapter = CompanySelectAdapter()
    val isEmpty = ObservableBoolean(true)
    private val companies = mutableListOf<Company>()

    init {
        createAdapter()
        registerListeners()
    }

    fun add(company: Company) {
        companies.add(company)
        adapter.notifyItemInserted(companies.size-1)
        isEmpty.set(false)
    }

    private fun createAdapter() {
        adapter.setCompanies(companies)
        adapter.setClickedListener {
            StateController.company = it
            onCompanySelectedListener(it)
        }
        binding.recyclerView.adapter = adapter
        binding.viewModel = this
    }
    private fun registerListeners() {
        binding.extendedFab.setOnClickListener {
            onAddCompanySelected()
        }
    }

}